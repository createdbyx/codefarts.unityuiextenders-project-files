﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EncapsulateChildren.cs" company="Codefarts">
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Codefarts.UnityUIExtenders
{
    using System;
    using UnityEngine;
    using UnityEngine.UI;

    /// <summary>
    /// The encapsulate children.
    /// </summary>
    [AddComponentMenu("Layout/Encapsulate Children", 152)]
    public class EncapsulateChildren : HorizontalOrVerticalLayoutGroup//, ILayoutSelfController
    {
        /// <summary>
        /// Start is called just before any of the Update methods is called the first time.
        /// </summary>
        protected override void Start()
        {
            this.childForceExpandHeight = false;
            this.childForceExpandWidth = false;
        }

        /// <summary>
        /// The calc along axis.
        /// </summary>
        /// <param name="axis">
        /// The axis.
        /// </param>
        /// <param name="isVertical">
        /// The is vertical.
        /// </param>
        protected void CalcAlongAxis(int axis, bool isVertical)
        {
            float combinedPadding = axis == 0 ? padding.horizontal : padding.vertical;

            var totalMin = combinedPadding;
            var totalPreferred = combinedPadding;
            var totalFlexible = 0f;

            float min = int.MaxValue;
            float max = int.MinValue;
            for (var i = 0; i < this.rectChildren.Count; i++)
            {
                var child = this.rectChildren[i];

                var rect = child.rect;
                var childPosition = child.localPosition;
                //Debug.Log(string.Format("{0} - {1} - min {2}x{3} - max {4}x{5} - sizedelta {6} - pos {7}",
                //     child.name,
                //     rect,
                //     rect.xMin,
                //     rect.yMin,
                //     rect.xMax,
                //     rect.yMax,
                //     child.sizeDelta,
                //     childPosition));

                var pivot = child.pivot;
                min = Math.Min(min, axis == 0 ? childPosition.x - (pivot.x * rect.width) : childPosition.y - (pivot.y * rect.height));
                max = Math.Max(max, axis == 0 ? childPosition.x + ((1f - pivot.x) * rect.width) : childPosition.y + ((1f - pivot.y) * rect.height));
            }

            totalPreferred = max; 
            // Debug.Log(string.Format("container - min {0} - max {1} - totalPreferred {2} - axis {3}", min, max, totalPreferred, axis));
            this.SetLayoutInputForAxis(totalMin, totalPreferred, totalFlexible, axis);
            if (axis == 0)
            {
                this.SetWidth(this.rectTransform, Math.Max(0, this.padding.left + totalPreferred + this.padding.right));
            }
            else
            {
                this.SetHeight(this.rectTransform, Math.Max(0, this.padding.top + -min + this.padding.bottom));
            }
        }

        /// <summary>
        /// The set size.
        /// </summary>
        /// <param name="trans">
        /// The trans.
        /// </param>
        /// <param name="newSize">
        /// The new size.
        /// </param>
        private void SetSize(RectTransform trans, Vector2 newSize)
        {
            var oldSize = trans.rect.size;
            var deltaSize = newSize - oldSize;
            trans.offsetMin = trans.offsetMin - new Vector2(deltaSize.x * trans.pivot.x, deltaSize.y * trans.pivot.y);
            trans.offsetMax = trans.offsetMax + new Vector2(deltaSize.x * (1f - trans.pivot.x), deltaSize.y * (1f - trans.pivot.y));
        }

        /// <summary>
        /// The set width.
        /// </summary>
        /// <param name="trans">
        /// The trans.
        /// </param>
        /// <param name="newSize">
        /// The new size.
        /// </param>
        private void SetWidth(RectTransform trans, float newSize)
        {
            this.SetSize(trans, new Vector2(newSize, trans.rect.size.y));
        }

        /// <summary>
        /// The set height.
        /// </summary>
        /// <param name="trans">
        /// The trans.
        /// </param>
        /// <param name="newSize">
        /// The new size.
        /// </param>
        private void SetHeight(RectTransform trans, float newSize)
        {
            this.SetSize(trans, new Vector2(trans.rect.size.x, newSize));
        }

        /// <summary>
        /// The on transform children changed.
        /// </summary>
        public void OnTransformChildrenChanged()
        {
            LayoutRebuilder.MarkLayoutForRebuild(this.rectTransform);
        }

        /// <summary>
        /// The calculate layout input horizontal.
        /// </summary>
        public override void CalculateLayoutInputHorizontal()
        {
            base.CalculateLayoutInputHorizontal();
            this.CalcAlongAxis(0, true);
        }

        /// <summary>
        /// The calculate layout input vertical.
        /// </summary>
        public override void CalculateLayoutInputVertical()
        {
            this.CalcAlongAxis(1, true);
        }

        /// <summary>
        /// The set layout horizontal.
        /// </summary>
        public override void SetLayoutHorizontal()
        {
        }

        /// <summary>
        /// The set layout vertical.
        /// </summary>
        public override void SetLayoutVertical()
        {
        }
    }
}
