﻿namespace Codefarts.UnityUIExtenders
{
    using UnityEngine;
    using UnityEngine.EventSystems;
    using UnityEngine.UI;

    [ExecuteInEditMode]
    [AddComponentMenu("Layout/Automatic Layout Rebuilder", 153)]
    public class AutomaticLayoutRebuilder : UIBehaviour
    {
        public RectTransform rectTransform;

        public bool RebuildOnPositionChange;

        private Vector3 oldPosition;

        private Transform transformReference;

        /// <summary>
        /// Start is called just before any of the Update methods is called the first time.
        /// </summary>
        public void Start()
        {
            this.transformReference = this.transform;
        }


        /// <summary>
        /// Update is called every frame, if the MonoBehaviour is enabled.
        /// </summary>
        public void Update()
        {
            if (this.RebuildOnPositionChange && this.oldPosition != this.transformReference.localPosition)
            {
                LayoutRebuilder.MarkLayoutForRebuild(this.rectTransform);
                this.oldPosition = this.transformReference.localPosition;
            }
        }

        public void OnRectTransformDimensionsChange()
        {
            LayoutRebuilder.MarkLayoutForRebuild(this.rectTransform);
        }
    }
}